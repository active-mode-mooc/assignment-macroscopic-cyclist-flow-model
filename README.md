# Assignment Macroscopic Cyclist Flow Model 

Assignment 4.2 of the Active Mode MOOC.

Some explanation will follow later...

## How to use this repository? ##
Clone the repository to your local machine:

```
git clone --recursive https://gitlab.tudelft.nl/active-mode-mooc/assignment-macroscopic-cyclist-flow-model
```

The `--recursive` argument indicates that the content of the referenced [shared](https://gitlab.tudelft.nl/active-mode-mooc/shared) repository is imported. 
Modifications made within the *shared* folder are associated with the [shared](https://gitlab.tudelft.nl/active-mode-mooc/shared) repository and thus have to committed on this repository.

## Get latest version of shared repository ##
First of all, make sure that you don't have any open changes in your repository or in the [shared](https://gitlab.tudelft.nl/active-mode-mooc/shared) repository.

Then, to import the latest version of the [shared](https://gitlab.tudelft.nl/active-mode-mooc/shared) repository, the next command should be executed:
```
git submodule update --remote
```
In order to save the version update of the [shared](https://gitlab.tudelft.nl/active-mode-mooc/shared) repository to the assignment repository, commit the unstaged folder version change, either using your preferred GUI, or from the terminal: 
```
git add --all
git commit -m "Updated shared repo"
```

## How to embed the notebook? ##

The repository can be run with MyBinder under the following URL's:

**Classic Notebook** interface: 
```
https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.tudelft.nl%2Factive-mode-mooc%2Fassignment-macroscopic-cyclist-flow-model/master?filepath=Assignment%20Macroscopic%20Cycle%20Flow%20Model.ipynb
```

**Nteract** interface:
```
https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.tudelft.nl%2Factive-mode-mooc%2Fassignment-macroscopic-cyclist-flow-model/master?urlpath=nteract/tree/Assignment%20Macroscopic%20Cycle%20Flow%20Model.ipynb
```

**JupyterLab** interface
```
https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.tudelft.nl%2Factive-mode-mooc%2Fassignment-macroscopic-cyclist-flow-model/master?urlpath=lab/tree/Assignment%20Macroscopic%20Cycle%20Flow%20Model.ipynb
```

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.tudelft.nl%2Factive-mode-mooc%2Fassignment-macroscopic-cyclist-flow-model/master?filepath=Assignment%20Macroscopic%20Cycle%20Flow%20Model.ipynb)

